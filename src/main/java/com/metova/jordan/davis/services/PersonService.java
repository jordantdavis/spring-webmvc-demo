package com.metova.jordan.davis.services;

import com.metova.jordan.davis.models.Person;

import java.util.List;

public interface PersonService {
    List<Person> getAll();
    Person get(String id);
    Person save(Person Person);
    Person update(Person Person);
    void delete(String id);
}
