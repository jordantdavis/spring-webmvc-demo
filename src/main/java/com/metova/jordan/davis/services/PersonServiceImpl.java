package com.metova.jordan.davis.services;

import com.metova.jordan.davis.models.Person;
import com.metova.jordan.davis.repository.PersonRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    private final PersonRepository repository;

    @Inject
    public PersonServiceImpl(final PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public List<Person> getAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public Person get(String id) {
        return repository.findOne(id);
    }

    @Override
    @Transactional
    public Person save(Person person) {
        return repository.save(person);
    }

    @Override
    @Transactional
    public Person update(Person person) {
        return repository.save(person);
    }

    @Override
    @Transactional
    public void delete(String id) {
        repository.delete(id);
    }
}
