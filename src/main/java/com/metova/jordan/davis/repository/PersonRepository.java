package com.metova.jordan.davis.repository;

import com.metova.jordan.davis.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, String> {
}
