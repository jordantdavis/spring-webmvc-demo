package com.metova.jordan.davis.exceptions;

public class PersonNotFoundException extends RuntimeException {
    public PersonNotFoundException() {
        super("Person not found.");
    }
}
