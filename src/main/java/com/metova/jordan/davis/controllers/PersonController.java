package com.metova.jordan.davis.controllers;

import com.metova.jordan.davis.exceptions.PersonNotFoundException;
import com.metova.jordan.davis.models.ApiError;
import com.metova.jordan.davis.models.Person;
import com.metova.jordan.davis.services.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/people")
public class PersonController {
    private final PersonService personService;

    @Inject
    public PersonController(final PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Person> getAllPersons() {
        return personService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Person getPersonById(@PathVariable("id") String id) {
        Person person = personService.get(id);

        if (person == null) {
            throw new PersonNotFoundException();
        }

        return person;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createPerson(@RequestBody @Valid final Person person) {
        personService.save(person);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePerson(@PathVariable("id") String id, @RequestBody @Valid final Person person) {
        Person existingPerson = personService.get(id);

        if (existingPerson == null) {
            throw new PersonNotFoundException();
        }

        person.setId(id);
        personService.update(person);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePerson(@PathVariable("id") String id) {
        Person existingPerson = personService.get(id);

        if (existingPerson == null) {
            throw new PersonNotFoundException();
        }

        personService.delete(id);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleInvalidRequestBody() {
        return new ApiError("Invalid request body.");
    }

    @ExceptionHandler(PersonNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handlePersonNotFoundException(PersonNotFoundException exception) {
        return new ApiError(exception.getMessage());
    }
}
